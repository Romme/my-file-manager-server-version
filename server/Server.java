package server;



import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private ServerSocket serverSock;
    private Socket clientSock;
    private PrintWriter out;
    private BufferedReader in;
    private String rootFolderName = "FileManager";
    private String rootFolderPath = "C:/";

    public void start(int port) throws IOException{
        serverSock = new ServerSocket(port);
        //JSONParser parser = new JSONParser();

        while (true) {
            System.out.println("Wait for new connection...");
            ClientTask clientTask = new ClientTask(serverSock.accept()); //ждет пока не придет клиент
            System.out.println("Start new Thread");
            new Thread(clientTask).start();
        }
        //serverSock.close();
    }

    private void createRootFolder(){
        File newFolder;
        newFolder = new File(rootFolderPath, rootFolderName);
        if(!newFolder.exists()){
            newFolder.mkdir();
        }
    }
    public String getRootFolderName(){
        return rootFolderName;
    }

    public String getRootFolderPath(){
        return rootFolderPath;
    }

    public static void  main(String[] args) throws IOException {

        Server server = new Server();
        server.createRootFolder();
        server.start(5000);
    }
}
