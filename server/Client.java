package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket sock;
    private PrintWriter out;
    private BufferedReader in;

    private String rootPath;

    public Client(String host, int port) throws IOException {
        sock = new Socket(host, port);
        out = new PrintWriter(sock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
    }

    public String send(String passw) throws IOException{
        String message = "";
        out.println(passw);
        String response =  in.readLine();
        return response;
    }

    public void stop() throws IOException {
        in.close();
        out.close();
        sock.close();
    }
    public static void pressAnyKeyToContinue(){
        System.out.println("Press Enter key to continue...");
        try {
            System.in.read();
        }
        catch (Exception e)
        {}
    }

    public  String getLine() throws IOException {
        return in.readLine();
    }

    public static void main(String[] args) throws IOException{
        Client client = new Client("127.0.0.1", 5000);


        System.out.println(client.getLine());

        Scanner in = new Scanner(System.in);
        String response = client.send(in.nextLine());
        System.out.println(response);


        client.stop();
    }
}
