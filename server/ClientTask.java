package server;


import java.io.*;
import java.lang.reflect.Array;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class ClientTask implements Runnable{
    private Socket clientSock;
    private PrintWriter out;
    private BufferedReader in;

    private String rootFolderName = "FileManager";
    private String rootFolderPath = "C:/";

    private String password = "123";

    public ClientTask(Socket client) throws IOException {
        this.clientSock = client;
        out = new PrintWriter(clientSock.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSock.getInputStream()));
    }

    @Override
    public void run() {
       // JSONParser parser = new JSONParser();
        System.out.println("Opened connection to " + clientSock.getRemoteSocketAddress());
        out.println("Enter password: ");


        while (!clientSock.isOutputShutdown()) {
            String data = null;
            //JSONObject answer = new JSONObject();
            String answer = "";


            try {
                data = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (data == null){
                break;
            }
           // JSONObject message = (JSONObject) parser.parse(data);
            if (data.equals("passw")) {
                String passw = "";
                out.println("200");
                try {
                    passw = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(passw.equals(password)){
                    out.println("Password is correct!");
                }
                else{
                    out.println("Wrong passord!");
                }

            }
            else if(data.equals("is file")){
                out.println("*200");
                String filePath = "";
                try {
                    filePath = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File file = new File(filePath);
                if(file.isFile()){
                    out.println("true");
                }
                else{
                    out.println("false");
                }
            }
            else if(data.equals("delete")) {
                out.println("*200");
                String selectedObject = "";
                String currentDir = "";
                try {
                    selectedObject = in.readLine();
                    currentDir = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                deleteDir(new File(currentDir, selectedObject));

            }
            else if(data.equals("new folder")) {
                out.println("*200");
                String newFolderName = "";
                String currentDir = "";
                try {
                    newFolderName = in.readLine();
                    currentDir = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File newFolder = new File(currentDir, newFolderName);
                if(!newFolder.exists()){
                    newFolder.mkdir();
                }
            }
            else if(data.equals("new file")) {
                out.println("*200");
                String newFileName = "";
                String currentDir = "";
                try {
                    newFileName = in.readLine().split(".")[0];
                    currentDir = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File newFile = new File(currentDir, newFileName);
                if(!newFile.exists()){
                    try {
                        newFile.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            else if(data.equals("read file")) {
                out.println("*200");
                String fileName = "";
                String currentDir = "";
                try {
                    fileName = in.readLine();
                    out.println("*200");
                    currentDir = in.readLine();
                    out.println("*200");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File newFile = new File(currentDir, fileName);
                Scanner f;
                String result = "";
                if(newFile.exists()){
                    if(newFile.isFile()){
                        try {
                            f = new Scanner(newFile);
                            while(f.hasNext()){
                                out.println(f.nextLine()+" ");
                            }
                            f.close();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
                out.println("*end*");

            }
            else if(data.equals("edit file")) {
                out.println("*200");
                String newText = "";
                String filePath = "";
                try {
                    newText = in.readLine();
                    filePath = in.readLine();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File file = new File(filePath);
                if(file.exists()){
                    if(file.isFile()){
                        FileWriter fileWriter;
                        try {
                            fileWriter = new FileWriter(file);
                            fileWriter.append(newText);
                            fileWriter.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            else if(data.equals("rename")) {
                out.println("*200");
                String newName = "";
                String currentDir = "";
                String selectedObject = "";
                try {
                    newName = in.readLine();
                    out.println("*200");
                    currentDir = in.readLine();
                    out.println("*200");
                    selectedObject = in.readLine();
                    out.println("*200");
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File renameFile = new File(currentDir, selectedObject);
                renameFile.renameTo(new File(currentDir, newName));
            }

            else if(data.equals("get root name")){
                answer = rootFolderName;
            }
            else if(data.equals("get all in root")) {
                out.println("*200");
                File selectedFile = new File(rootFolderPath + rootFolderName + "/");
                if (selectedFile.isDirectory()) {
                    String[] rootStr = selectedFile.list();
                    for (String str : rootStr) {
                        File checkObject = new File(selectedFile.getPath(), str);
                        if (!checkObject.isHidden()) {
                            out.println(str);
                        }
                    }
                }
                out.println("*end*");
                String currentPath = rootFolderPath + rootFolderName + "/";
                out.println(currentPath);
            }
            else if(data.equals("get all in folder")) {
                out.println("*200");
                String selectedObject = "";

                String oldPath = "";
                try {
                    selectedObject = in.readLine();
                    out.println("*200");
                    oldPath = in.readLine();
                    out.println("*200");
                } catch (IOException e) {
                    e.printStackTrace();
                }

                File selectedFile;
                String fullPath = oldPath;
                if(selectedObject.equals("*getPrev")){
                    File oldPathFile = new File(oldPath);
                    File rootPathFile = new File(rootFolderPath, rootFolderName);
                    if(rootPathFile.getPath().equals(oldPathFile.getPath())){
                        selectedFile = new File(oldPath);
                    }
                    else {
                        selectedObject = new File(oldPath).getParent();
                        selectedFile = new File(selectedObject);
                    }
                }
                else{
                    selectedFile = new File(fullPath + "/" + selectedObject);
                }

                String currentPath = oldPath;
                if (selectedFile.isDirectory()) {
                    currentPath = selectedFile.toString();
                    String[] rootStr = selectedFile.list();
                    for (String str : rootStr) {
                        File checkObject = new File(selectedFile.getPath(), str);
                        if (!checkObject.isHidden()) {
                            out.println(str);
                        }
                    }
                }

                out.println("*end*");
                out.println(currentPath);
            }



            else {
                answer = "Wrong message";
                //answer.put("error", "Wrong password!");
            }


            out.println(answer);
        }
        System.out.println("Close connection "+ clientSock.getRemoteSocketAddress());
        try {
            in.close();
            out.close();
            clientSock.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteDir(File file){
        File[] objects = file.listFiles();
        if(objects != null){
            for(File f : objects){
                deleteDir(f);
            }
        }
        file.delete();
    }


    private String toFullPath(List<String> file){
        String listPart = "";
        for(String str : file){

            listPart = listPart + str;
            int index = str.indexOf("\\");
            if(str.indexOf("/") == -1){
                listPart = listPart + "/";
            }
        }
        return listPart;
    }

}

